/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theerawen.receiptdao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OMEN
 */
public class TestUpdateUser {
     public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "UPDATE user SET name = ?,tel = ? ,password = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Pum siri");
            stmt.setString(2, "0970126919");
            stmt.setString(3, "Bankkoklim@1333");
            stmt.setInt(4, 2);
            int row = stmt.executeUpdate();
            System.out.println("Affect row" + row);
        
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
       db.close();

    }
}
